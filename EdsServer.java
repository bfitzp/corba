import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;

import org.omg.CORBA.ORB;
import org.omg.CosNaming.NameComponent;
import org.omg.CosNaming.NamingContext;
import org.omg.CosNaming.NamingContextHelper;

import eds.deptdetails;
import eds.directory;
import eds.directoryHelper;
import eds.userdetails;

public class EdsServer {

	public static ArrayList<deptdetails> readDepartments(String filename) {
		ArrayList<deptdetails> departments = new ArrayList<deptdetails>();

		try {
			BufferedReader reader = new BufferedReader(new FileReader(filename));
			String line = null;
			while ((line = reader.readLine()) != null) {
				deptdetails details = new deptdetails(); 

				Scanner scanner = new Scanner(line);
				scanner.useDelimiter(",");

				details.name = scanner.next();
				details.location = scanner.next();
				details.managerName = scanner.next();

				departments.add(details);

				scanner.close();
			}
		} catch (Exception e){
			System.out.println(e.getMessage());
		}

		return departments;
	}

	public static ArrayList<userdetails> readUsers(String filename) {
		ArrayList<userdetails> users = new ArrayList<userdetails>();

		try {
			BufferedReader reader = new BufferedReader(new FileReader(filename));
			String line = null;
			while ((line = reader.readLine()) != null) {
				
				userdetails details = new userdetails(); 

				Scanner scanner = new Scanner(line);
				scanner.useDelimiter(",");

				details.name = scanner.next();
				details.tel = scanner.next();
				details.email = scanner.next();
				details.location = scanner.next();

				users.add(details);

				scanner.close();
			}
		} catch (Exception e){
			System.out.println(e.getMessage());
		}

		return users;
	}

	public static void main(String args[]) {
		try{
			// create and initialize the ORB
			ORB orb = ORB.init(args, null);

			// read in department and user details
			DirectoryServant directoryServant = new DirectoryServant();
			ArrayList<deptdetails> departments = readDepartments("deptdetails.csv");
			ArrayList<userdetails> users = readUsers("userdetails.csv");
			
			for (deptdetails details : departments) {
				// purposely adding all users to each department for the sake of simplicity
				directoryServant.departments.add(new DepartmentServant(details, directoryServant.users));
			}
			
			int index = 0;
			for (userdetails details : users) {
				// hardcoding the department name to "Mathematics" for all users for the sake of simplicity
				directoryServant.users.add(new UserServant(details, "Mathematics", index++, directoryServant.departments));
			}

			// get the root naming context
			org.omg.CORBA.Object objRef = 
					orb.resolve_initial_references("NameService");
			NamingContext ncRef = NamingContextHelper.narrow(objRef);

			directory directoryRef = directoryHelper.narrow(directoryServant);

			// bind the Object Reference in Naming
			NameComponent nc = new NameComponent("Eds", "");
			NameComponent path[] = {nc};
			ncRef.rebind(path, directoryRef);
			System.out.println("EdsServer ready and waiting ...");

			// wait for invocations from clients
			orb.run();
		}

		catch (Exception e) {
			System.err.println("ERROR: " + e);
			e.printStackTrace(System.out);
		}

		System.out.println("HelloServer Exiting ...");

	}
}