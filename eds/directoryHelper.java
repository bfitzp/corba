package eds;


/**
* eds/directoryHelper.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from eds.idl
* 18 October 2013 19:16:02 o'clock IST
*/

abstract public class directoryHelper
{
  private static String  _id = "IDL:eds/directory:1.0";

  public static void insert (org.omg.CORBA.Any a, eds.directory that)
  {
    org.omg.CORBA.portable.OutputStream out = a.create_output_stream ();
    a.type (type ());
    write (out, that);
    a.read_value (out.create_input_stream (), type ());
  }

  public static eds.directory extract (org.omg.CORBA.Any a)
  {
    return read (a.create_input_stream ());
  }

  private static org.omg.CORBA.TypeCode __typeCode = null;
  synchronized public static org.omg.CORBA.TypeCode type ()
  {
    if (__typeCode == null)
    {
      __typeCode = org.omg.CORBA.ORB.init ().create_interface_tc (eds.directoryHelper.id (), "directory");
    }
    return __typeCode;
  }

  public static String id ()
  {
    return _id;
  }

  public static eds.directory read (org.omg.CORBA.portable.InputStream istream)
  {
    return narrow (istream.read_Object (_directoryStub.class));
  }

  public static void write (org.omg.CORBA.portable.OutputStream ostream, eds.directory value)
  {
    ostream.write_Object ((org.omg.CORBA.Object) value);
  }

  public static eds.directory narrow (org.omg.CORBA.Object obj)
  {
    if (obj == null)
      return null;
    else if (obj instanceof eds.directory)
      return (eds.directory)obj;
    else if (!obj._is_a (id ()))
      throw new org.omg.CORBA.BAD_PARAM ();
    else
    {
      org.omg.CORBA.portable.Delegate delegate = ((org.omg.CORBA.portable.ObjectImpl)obj)._get_delegate ();
      eds._directoryStub stub = new eds._directoryStub ();
      stub._set_delegate(delegate);
      return stub;
    }
  }

  public static eds.directory unchecked_narrow (org.omg.CORBA.Object obj)
  {
    if (obj == null)
      return null;
    else if (obj instanceof eds.directory)
      return (eds.directory)obj;
    else
    {
      org.omg.CORBA.portable.Delegate delegate = ((org.omg.CORBA.portable.ObjectImpl)obj)._get_delegate ();
      eds._directoryStub stub = new eds._directoryStub ();
      stub._set_delegate(delegate);
      return stub;
    }
  }

}
