package eds;

/**
* eds/deptdetailsHolder.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from eds.idl
* 18 October 2013 19:16:02 o'clock IST
*/

public final class deptdetailsHolder implements org.omg.CORBA.portable.Streamable
{
  public eds.deptdetails value = null;

  public deptdetailsHolder ()
  {
  }

  public deptdetailsHolder (eds.deptdetails initialValue)
  {
    value = initialValue;
  }

  public void _read (org.omg.CORBA.portable.InputStream i)
  {
    value = eds.deptdetailsHelper.read (i);
  }

  public void _write (org.omg.CORBA.portable.OutputStream o)
  {
    eds.deptdetailsHelper.write (o, value);
  }

  public org.omg.CORBA.TypeCode _type ()
  {
    return eds.deptdetailsHelper.type ();
  }

}
