import java.util.Scanner;

import org.omg.CORBA.ORB;
import org.omg.CosNaming.NameComponent;
import org.omg.CosNaming.NamingContext;
import org.omg.CosNaming.NamingContextHelper;

import eds.InvalidArgument;
import eds.InvalidLogin;
import eds.department;
import eds.deptdetails;
import eds.directory;
import eds.directoryHelper;
import eds.user;
import eds.userdetails;

public class EdsClient{
	
	static Scanner keyboard = new Scanner(System.in);
	
	static void printUserDetails(userdetails details){
		System.out.println("Name: " + details.name);
		System.out.println("Location: " + details.location);
		System.out.println("Email: " + details.email);
		System.out.println("Telephone: " + details.tel);
	}
	
	  public String name = null;
	  public String location = null;
	  public String managerName = null;
	
	static void printDepartmentDetails(deptdetails dept){
		System.out.println("Name: " + dept.name);
		System.out.println("Location: " + dept.location);
		System.out.println("Manager: " + dept.managerName);
	}

	static user handleLogin(directory directoryImpl) {
		user loggedinUser = null;
		while (loggedinUser == null) {
			System.out.println("Enter username:");
			String username = keyboard.nextLine();
			
			System.out.println("Enter password:");
			String password = keyboard.nextLine();
			
			try {
				// log user in
				loggedinUser = directoryImpl.login_user(username, password);
			} catch (InvalidLogin e) {
				System.out.println("\n" + e.reason + "\n");
			}
		}
		
		return loggedinUser;
	}
	
	static String displayOptions() {
		System.out.println("\n--------------------");
		System.out.println("--------------------\n");
		System.out.println("*** Options ***\n");
		System.out.println("1) Show all department details");
		System.out.println("2) Show particular department details");
		System.out.println("3) Show my details");
		System.out.println("4) Show particular user details");
		System.out.println("5) Show all user details");
		System.out.println("6) Update my details");
		System.out.println("7) Set password");
		System.out.println("8) Exit");
		System.out.println("Please choose an option:");
		return keyboard.nextLine();
	}
	
	static void run(user loggedinUser){
		try {
			Boolean exit = false;
			// show options
			while (exit == false){
				String option = displayOptions();
				
				System.out.println("\n--------------------");
				System.out.println("--------------------\n");		
				switch(option){
				case "1": {
					System.out.println("*** All Department Details ***\n");
					department dept[] = loggedinUser.getAllDepartments();
					
					for (int i = 0; i < dept.length; i++) {
						printDepartmentDetails(dept[i].details());
						if (i != dept.length-1){
							System.out.println("\n\n");
						}
					}
					break;
					}				
					case "2": {
						System.out.println("Enter the department name:");
						String deptName = keyboard.nextLine();
						
						try {
							deptdetails details = loggedinUser.getDepartment(deptName).details();
							
							System.out.println("*** Department Details ***\n");
							printDepartmentDetails(details);
						} catch(Exception e){
							System.out.println("\n*** Department with name: " + deptName +  " not found ***\n");
						}
						break;
					}
					case "3": {
						System.out.println("*** My Details ***\n");
						printUserDetails(loggedinUser.details());
						break;
					}
					case "4": {
						// all users are added to each department so just retrieve an arbitrary department
						department dept = loggedinUser.getDepartment("Mathematics");
						
						System.out.println("Enter the user's id:");
						int id = Integer.parseInt(keyboard.nextLine());
						
						try {
							userdetails details = dept.getUserDetails(id);
	
							System.out.println("*** Details of user with id: " + id +  " ***\n");
							printUserDetails(details);
						} catch(InvalidArgument e){
							System.out.println("\n" + e.reason + "\n");
						}
						break;
					}
					case "5": {
						// all users are added to each department so just retrieve an arbitrary department
						department dept = loggedinUser.getDepartment("Mathematics");
						
						userdetails users[] = dept.getAllUserDetails();		
	
						System.out.println("*** All User Details ***\n");
						for (int i = 0; i < users.length; i++) {
							printUserDetails(users[i]);
							if (i != users.length-1){
								System.out.println("\n\n");
							}
						}
						
						break;
					}
					case "6": {
						userdetails newDetails = new userdetails();
						
						System.out.println("*** Input new details ***\n");
						System.out.println("Enter name:");
						newDetails.name = keyboard.nextLine();
						System.out.println("Enter phone number:");
						newDetails.tel = keyboard.nextLine();
						System.out.println("Enter email address:");
						newDetails.email = keyboard.nextLine();
						System.out.println("Enter location:");
						newDetails.location = keyboard.nextLine();						
						
						loggedinUser.details(newDetails);
						System.out.println("\nYour details have been updated.");
						break;
					}
					case "7": {
						System.out.println("*** Set password ***\n");
						System.out.println("Enter your current password:");
						String currentPassword = keyboard.nextLine();
						System.out.println("Enter your new password:");
						String newPassword = keyboard.nextLine();
						
						try{
							loggedinUser.setPassword(currentPassword, newPassword);
							System.out.println("\nYour password has been updated.");
						} catch(InvalidArgument e){
							System.out.println("\n" + e.reason + "\n");
						}						
						break;
					}
					case "8": {
						System.out.println("Exiting...");
						System.exit(0);
						break;
					}
					default : System.out.println("You must choose an option between 1 - 8");

				}
			}
		}
		catch (Exception e) {
			System.out.println("ERROR : " + e) ;
			e.printStackTrace(System.out);
		}
	}
	
	public static void main(String args[]){

		try{
			// create and initialize the ORB
			ORB orb = ORB.init(args, null);

			// get the root naming context
			org.omg.CORBA.Object objRef = 
					orb.resolve_initial_references("NameService");
			NamingContext ncRef = NamingContextHelper.narrow(objRef);

			// resolve the Object Reference in Naming
			NameComponent nc = new NameComponent("Eds", "");
			NameComponent path[] = {nc};
			directory directoryImpl = directoryHelper.narrow(ncRef.resolve(path));

			System.out.println("\n*** Connected to EdsServer ***\n");
			
			user loggedinUser = handleLogin(directoryImpl);
			
			run(loggedinUser);
			
			
		} catch (Exception e) {
			System.out.println("ERROR : " + e) ;
			e.printStackTrace(System.out);
		}
	}
}
