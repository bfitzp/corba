// UserServant.java

import java.util.*;

import eds.*;

public class UserServant extends eds._userImplBase
{
  private userdetails mydetails;
  private String mydeptname;
  private int myid;
  private String password = "password";
  private ArrayList<DepartmentServant> departments;

  // Constructor
  public UserServant(userdetails mydetails, String mydeptname, int myid, ArrayList<DepartmentServant> departments)
  {
    this.mydetails = mydetails;
    this.mydeptname = mydeptname;
    this.myid = myid;
    this.departments = departments;
    this.password = "password";
  }

  // Methods required to implement user interface
  public userdetails details ()
  {
	  System.out.println(mydetails.email);
	  System.out.println(mydetails.name);
	  System.out.println(mydetails.location);
	  System.out.println(mydetails.tel);
	  return mydetails;
  }

  public void details (userdetails newDetails)
  {
	  this.mydetails = newDetails;
  }

  public String deptName ()
  {
	  return mydeptname;
  }

  public String username ()
  {
	  return mydetails.name;
  }

  public String password ()
  {
	  return password;
  }

  
  public int id ()
  {
	  return myid;
  }

  public department[] getAllDepartments ()
  {
	  DepartmentServant[] depts = new DepartmentServant[departments.size()];
	  
	  for (int i = 0; i < depts.length; i++) {
		  depts[i] = departments.get(i);
	  }
	  
	  return depts;
  }

  public department getDepartment (String name) throws InvalidArgument
  {
	  for (DepartmentServant department : departments) {
		  if (department.details().name.toLowerCase().equals(name.toLowerCase())){
			  return department;
		  }
	  }
	  
	  throw new InvalidArgument("*** Departmnet with name: " + name +  " not found ***");
  }

  public void setPassword (String oldpassword, String newpassword) throws InvalidArgument
  {
	  if (oldpassword.equals(this.password)) {
		  this.password = newpassword;
	  } else {
		  throw new InvalidArgument("*** Invalid input for current password ***");
	  }
  }

}
