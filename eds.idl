// CORBA IDL definitions for Employee Directory Service
// Compile using the idlj compiler with the -fall option
// To ensure interoperability, please do not change or edit this file

module eds {

struct userdetails {
	string name;
	string tel;
	string email;
  string location;
};

typedef sequence<userdetails> allemployees;

exception InvalidArgument { string reason; };

struct deptdetails {
  string name;
  string location;
  string managerName;
};

interface department
{
  // Encapsulates details about a particular department
	readonly attribute deptdetails details;

	allemployees getAllUserDetails();
	userdetails getUserDetails(in long id) raises (InvalidArgument);

};

typedef sequence<department> departments;

interface user
{
	// A user can modify their own entries / view other entries

	attribute userdetails details;
	readonly attribute string deptName;
	readonly attribute long id;
	readonly attribute string username;

	departments getAllDepartments();
	department getDepartment(in string name) raises (InvalidArgument);
	void setPassword(in string oldpassword, in string newpassword) raises (InvalidArgument);

};

typedef sequence<user> users;

exception InvalidUsername { string reason; };

exception InvalidLogin { string reason; };

interface directory
{
	// top-level interface that facilitates user /
	// manager logins - the server will create
	// a single instance of

	user login_user(in string username, in string password) raises (InvalidLogin);
};

}; // End module eds