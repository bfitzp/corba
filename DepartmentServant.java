// DepartmentServant.java
import java.util.*;
import eds.*;

public class DepartmentServant extends _departmentImplBase
{

  private deptdetails mydetails;
  private ArrayList<UserServant> users;

  // Constructor
  public DepartmentServant(deptdetails mydetails, ArrayList<UserServant> users)
  {
    this.mydetails = mydetails;
    this.users = users;
  }

  // Methods required to implement department interface
  public deptdetails details ()
  {
	  return mydetails;
  }

  public userdetails[] getAllUserDetails ()
  { 
	  userdetails[] details = new userdetails[users.size()];
	  
	  for (int i = 0; i < details.length; i++) {
		  details[i] = users.get(i).details();
	  }
	  
	  return details;
  }

  public userdetails getUserDetails (int id) throws InvalidArgument
  {
	  for (UserServant user : users) {
		  if (user.id() == id){
			  return user.details();
		  }
	  }
	  
	  throw new InvalidArgument("*** User with id: " + id +  " not found ***");
  }
}
