// DirectoryServant.java
import java.util.*;
import eds.*;

public class DirectoryServant extends eds._directoryImplBase
{

  // Use lists to store application state
  ArrayList<UserServant> users = new ArrayList<UserServant>();
  ArrayList<DepartmentServant> departments = new ArrayList<DepartmentServant>();

  // Methods required to implement directory interface
  public user login_user (String username, String password) throws InvalidLogin
  {
	  for (UserServant user : users) {
		  if (username.equals(user.username())){
			  if (password.equals(user.password())) {
				  return user;
			  }
		  }
	  }

	  throw new InvalidLogin("Invalid login details, please try again");
  }
  
}
